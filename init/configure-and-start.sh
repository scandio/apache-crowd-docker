#!/bin/bash

set -e

# Set default environment variables
export APACHE_PROXY_URL=${APACHE_PROXY_URL:=/}

export CROWD_ACCEPT_SSO=${CROWD_ACCEPT_SSO:=On}
export CROWD_CREATE_SSO=${CROWD_CREATE_SSO:=On}
export CROWD_AUTH_ENCODING=${CROWD_AUTH_ENCODING:=UTF-8}
export CROWD_TIMEOUT=${CROWD_TIMEOUT:=5}
export CROWD_CACHE_MAX_AGE=${CROWD_CACHE_MAX_AGE:=120}
export CROWD_CACHE_MAX_ENTRIES=${CROWD_CACHE_MAX_ENTRIES:=1000}
export CROWD_AUTHORITIVE=${CROWD_AUTHORITIVE:=On}

# Print used environment variables
echo "Using environment variables:"
echo "---------------------------------------------------------"
echo "> APACHE_SSL_CERTIFICATE_FILE=${APACHE_SSL_CERTIFICATE_FILE}"
echo "> APACHE_SSL_CERTIFICATE_KEY_FILE=${APACHE_SSL_CERTIFICATE_KEY_FILE}"
echo "> APACHE_PROXY_URL=${APACHE_PROXY_URL}"
echo "> APACHE_ORIGIN_URL=${APACHE_ORIGIN_URL}"
echo ">"
echo "> CROWD_REALM_NAME=${CROWD_REALM_NAME}"
echo "> CROWD_APP_NAME=${CROWD_APP_NAME}"
echo "> CROWD_APP_PASSWORD=${CROWD_APP_PASSWORD//*/\*}"
echo "> CROWD_URL=${CROWD_URL}"
echo "> CROWD_ACCEPT_SSO=${CROWD_ACCEPT_SSO}"
echo "> CROWD_CREATE_SSO=${CROWD_CREATE_SSO}"
echo "> CROWD_AUTH_ENCODING=${CROWD_AUTH_ENCODING}"
echo "> CROWD_TIMEOUT=${CROWD_TIMEOUT}"
echo "> CROWD_CACHE_MAX_AGE=${CROWD_CACHE_MAX_AGE}"
echo "> CROWD_CACHE_MAX_ENTRIES=${CROWD_CACHE_MAX_ENTRIES}"
echo "> CROWD_AUTHORITIVE=${CROWD_AUTHORITIVE}"
echo

# Import SSL certificate(s) if configured
echo "Importing SSL certificates:"
echo "---------------------------------------------------------"
if [ "$IMPORTCERT" = "true" ]; then
        cp ${IMPORTCERTPATH}/*.crt /usr/local/share/ca-certificates
	update-ca-certificates
	echo "> Installed CA certificates from ${IMPORTCERTPATH}"
else
	echo "> No SSL certificates to import"
fi
echo

# Prepare Apache2 configuration
echo "Preparing Apache configuration:"
echo "---------------------------------------------------------"

echo "Disabling Apache default site:"
a2dissite 000-default | sed 's/^/> /'
echo

echo "Enabling Apache modules:"
a2enmod rewrite proxy proxy_http ssl headers | sed 's/^/> /g'
echo

echo "Enabling Apache site 'reverse-proxy':"
echo "> Creating configuration for site reverse-proxy"
envsubst \
    < /root/docker/apache2/reverse-proxy.conf.template \
    > /etc/apache2/sites-available/reverse-proxy.conf
a2ensite reverse-proxy | sed 's/^/> /g'
echo

echo "Copying Apache configuration files:"
echo "> Copying security.conf"
    cp /root/docker/apache2/conf/security.conf /etc/apache2/conf-available/security.conf
echo "> Copying ssl.conf"
    cp /root/docker/apache2/conf/ssl.conf /etc/apache2/mods-available/ssl.conf
echo

echo "Starting Apache:"
echo "---------------------------------------------------------"
exec /usr/sbin/apache2ctl -D FOREGROUND
